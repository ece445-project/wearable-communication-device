# Yihan Notebook
* [references](#references)
* [searching STT](#SearchSTT)
* [searching TTS](#SearchTTS)
* [searching keyboard api](#SearchKeyboardApi)
* [testing TTS & STT localy](#TestModuleLocal)
* [testing TTS on pi](#TestTTSonPi)
* [testing STT on pi](#TestSTTonpi)
* [integrating STT and TTS](#integrating)
* [searching UI](#SearchUI)
* [writing UI](#WriteUI)

# references <a name= "references"></a>
https://circuitdigest.com/microcontroller-projects/best-text-to-speech-tts-converter-for-raspberry-pi-espeak-festival-google-tts-pico-and-pyttsx3  

https://www.openhab.org/addons/voice/picotts/

https://github.com/sevangelatos/py-ttspico

https://pypi.org/project/py-picotts/

https://wiki.python.org/moin/TkInter

https://realpython.com/python-gui-tkinter/

https://www.youtube.com/watch?v=YCyuZM454_I

https://github.com/alphacep/vosk-api

https://github.com/openstenoproject/plover

# Sep 18th, 2021
# searching STT <a name= "SearchSTT"></a>
SPEECH TO TEXT MODULE
Reference: https://www.youtube.com/watch?v=YCyuZM454_I

Create an account for IBM Cloud to get APIs we want

Go to AI category find Speech to Text Api (free for 500 minutes/month)

Speech to Text-p1

{

  "apikey": "8s82uiVwyqw3uxpaBBCG8HYer5BiOWbdk6ptRu8TUS4G",

  "iam_apikey_description": "Auto-generated for key 84bc339c-f2e8-4842-b818-6e551e3e3242",

  "iam_apikey_name": "Auto-generated service credentials",
  
  "iam_role_crn": "crn:v1:bluemix:public:iam::::serviceRole:Manager",

  "iam_serviceid_crn": "crn:v1:bluemix:public:iam-identity::a/
  
  838aefd525b14310bd2e48248d90954d::serviceid:ServiceId-3f928e3b-1f59-4566-877e-5858f677c65b",

  "url": "https://api.us-east.speech-to-text.watson.cloud.ibm.com/instances/de2dec71-a47d-4e29-b2a7-ac4075716dde"

}

(1) GET API key:

API key: 8s82uiVwyqw3uxpaBBCG8HYer5BiOWbdk6ptRu8TUS4G

Region: us-east

(2) Access and clone the Live Speech to Text Code: https://github.com/IBM/watson-streaming-stt 

(3) Based on the video we need to Install dependencies by using "pip install -r requirements.txt"

	However, there was an error occurred (return errors: ERROR: Command errored out with exit status 1)
	error occurred when we try to pip install PyAudio
	for MAC users, try:
	according to https://stackoverflow.com/questions/61290821/error-command-errored-out-with-exit-status-1-while-installing-pyaudio 
	we need to use Homebrew for Mac computer 
	install Homebrew based on "https://www.addictivetips.com/mac-os/install-homebrew-on-macos-catalina/" 
	
	for Windows users, try:
	"pip install pipwin"
	"pipwin install pyaudio"

	PROBLEM SOLVED!!! (Successfully installed pyAudio-0.2.11)

(4) Update setup/config with APIKEY

	- change "speech.cfg.example" to "speech.cfg"
	- set APIKEY
	- set region

Models: https://cloud.ibm.com/docs/speech-to-text?topic=speech-to-text-models

(Include many valid models we can use for different languages)

(You can change the language by easily change the model in LINE 175 in transcribe.py)

(5) Run the code 

"python transcribe.py -t 20"

20 represent the time you want to record to transfer from speech to text

# Sep 24th, 2021
# searching TTS <a name= "SearchTTS"></a>

TEXT TO SPEECH MODULE

Reference: 

https://www.youtube.com/watch?v=-nHncxpN_Qc

https://www.youtube.com/watch?v=kfqpFKdDVMU

# searching keyboard api <a name= "SearchKeyboardApi"></a>

try to understand concept of stenographic keyboard and try to figure out how to do the software part part the stenographic keyboard

relevant source: 

https://softhruf.love 

github for plover: https://github.com/openstenoproject/plover/wiki/Beginner's-Guide:-Get-Started-with-Plover

another github for stenokey: https://github.com/mike-ady/Stenokey

Georgi Steno: https://www.gboards.ca/product/georgi

github for plover: https://github.com/openstenoproject/plover/releases

github for keyboard pcb:https://github.com/ruiqimao/keyboard-pcb-guide 

# Sep 28th, 2021

Start writing the Design Document (requirement and verification in the design part)

Found the previous speech to text module cannot work without internet

Expected solution: either change the code or find another api/library

Relative resources:

https://www.youtube.com/watch?v=YereI6Gn3bM 

Founded appropriate STT module and TTS module

STT: 
	Vosk
	https://github.com/alphacep/vosk-api
	https://alphacephei.com/vosk/

TTS:
	pyttsx3
	https://pypi.org/project/pyttsx3/


# Sep 30th, 2021
Having the design document check

Feedback: need to change high-level requirement

we also need to change RV table we write RV table for every signal component 

what we should do is write higher level requirements for each subunit

finishing the design document 

add cost for different parts (physical components and labor)

mainly modifying RV table for each subunit

add more supporting figures that can effectively show technical detail of our design

# Oct 2nd, 2021
# testing TTS & STT localy <a name= "TestModuleLocal"></a>

run Vosk and pyttsx3 on my laptop

Vosk can get pretty high accuracy rate and voice message transferred by pyttsx3 has a pretty good sound quality


# Oct 6th, 2021

Read and understand the logic for vosk module, because we need to know how to integrate modules toagther without changing any existing functionalities. 

----------------------------------------------

<img src="Notebooks/Yihan/STT_flowchart.jpeg" width="250">          <img src="Notebooks/Yihan/Vosk_STT_Code.png" width="400">

<pre>
Flowchart for Vosk					Code for detect signal in Vosk
</pre>

----------------------------------------------


# Oct 13th, 2021
# testing TTS on pi <a name= "TestTTSonPi"></a>

After our raspberry pi arrived we tested TTS on the rasoberry pi. However, we found out that even the module has a really good sound quality on my laptop, when we run it on rapberry pi, it causes some problem. In order to run the code, we need to import espeak for the raspberry pi operating system, but the sound quality for espeak is pretty bad. Therefore, we researched other modules listed in following table. After rating the sound quality with one person wearing the headphone, and the other random typing messages, we decided to use Pico TTS. 

----------------------------------------------

<img src="Notebooks/Yihan/TTS_Table.png" width="600">

__TTS Modules Table__

----------------------------------------------


# Oct 16th, 2021
# testing STT on pi <a name= "TestSTTonpi"></a>

We first try to test the STT module using same headphone with audio jack, but it didn't output any messages to the terminal. 
We found out raspberry pi will only delivery output through audio jack, and it will not take any input from that port. 
With an USB microphone, the vosk STT module works perfectly. 

# Oct 24th, 2021
# integrating STT and TTS <a name= "integrating"></a>

For now, we have two seperate python file for those two modules: STT and TTS, and we can only run one file at a time by runnning "python 'name of python file' " on the terminal. It will be unflexible for user to switch modules. Therefore, I decided to use an function to detect input from keyboard. When the user pressed '1', the terminal will start running TTS module. By pressing 'q' when user are running TTS, the terminal will start running STT module. 

----------------------------------------------

<img src="Notebooks/Yihan/integrating_STT.png" width="600">

__Code to detect whether we have '1' during STT__

----------------------------------------------

----------------------------------------------

<img src="Notebooks/Yihan/integrating_TTS.png" width="600">

__Code to detect whether we have 'q' during TTS__

----------------------------------------------

# Nov 8th, 2021
# searching UI <a name= "SearchUI"></a>

Instead of just switching those two moduels on terminals, we decide to create an user interface, which can help us maintain a communication log. The result trasnferred from STT and TTS will also become easier to distinguish with an user interface. 
After searching libraries we can used for creating an user interface, we decide to use Tkinter to create our user interface. 


# Nov 10th, 2021
# writing UI <a name= "WriteUI"></a>

The first things we need to do is create two binding keys for TTS module and STT module. We decide to use F1 to call TTS module and F2 to call STT module. Besides, the layout of user interface should have an entry which allow user to type and a frame which will display text transferred from speech and typed by user. 

----------------------------------------------

<img src="Notebooks/Yihan/ui_layout.png" width="600">

__User Interface Layout__

----------------------------------------------


# Nov 12th, 2021

Instead of using two keys for two modules, I changed the code to use one key swicth between TTS module and STT module. I decided to use F1 as the switching key. I also draw a flowchart for our user interface. 

----------------------------------------------

<img src="Notebooks/Yihan/UI_flowchart.jpeg" width="300">

__Flowchart for User Interface__

----------------------------------------------

----------------------------------------------

<img src="Notebooks/Yihan/ui_STT.png" width="400">          <img src="Notebooks/Yihan/ui_TTS.png" width="400">

<pre>
User interface when STT running					User interface when TTS running	
</pre>

----------------------------------------------

# Nov 15th, 2021

In order to maintain an communication log, I created an list of four unused labels. Those labels will be updated when we callled TTS or STT function. With this modification, the user interface can help us keep tracking four latest sentences transferred from STT or typed by the user. In order to differentiate STT and TTS, we also use different colors for different function. 

For STT ouput, we use following code to display blue text

	llist[count].config(text = pres, font = "Helvetica 24", wraplength = 650, fg = "blue")

For TTS ouput, we use following code to display black text

	llist[count].config(text=value, font = "Helvetica 24", wraplength = 650, fg = 'black')

----------------------------------------------

<img src="Notebooks/Yihan/communication_log.png" width="300">

__Label used for communication log__

----------------------------------------------

# Nov 20th, 2021

Consider people may not familiar with this device, we decide to add another binding key to an intro_TTS function, which will play following context through speach when the user pressing F3.

	value = 'Hello this is group nineteen '\
            'and this device can help people who have problem with hearing or speaking '\
            'if you can hold this display and speak to the microphone '\
            'your voive messages will be transfered and showed on the display'

At the end, we have five binding keys in total. 

----------------------------------------------

<img src="Notebooks/Yihan/binding_keys.png" width="300">

__Binding Keys for User Interface__

----------------------------------------------
