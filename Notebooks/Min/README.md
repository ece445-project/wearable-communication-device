# Minho Table of Contents
* [2021-09-16 Considering power requirements.](#1)
* [2021-09-18 DIY power bank?](#2)
* [2021-10-18 First PCB assembly ](#3)
* [2021-10-21 testing charger](#4)
* [2021-10-25 Redesigning PCB](#5)
* [2021-10-30 Reordering some parts.](#6)
* [2021-11-02 Testing Charger](#7)
* [2021-11-03 Testing Speaker and Microphone.](#8)
* [2021-11-04 Audio amplifier](#9)
* [2021-11-05 USB streo sound adapter?](#10)
* [2021-11-10 new PCB parts and tests](#11)
* [2021-11-11 DIY amplifier](#12)
* [2021-11-12 DIY microphone ](#13)
* [2021-11-15 Voltage Regulator](#14)
* [2021-11-25 Voltage Regulator 2](#15)
* [2021-11-26 Voltage Regulator 3](#16)
* [2021-11-28 Speaker Amplifier Test](#17)
* [2021-11-28 Completed Speaker](#18)

# 2021-09-16 Considering power requirements.<a name= "1">

Power Requirements

Power 
- Power Bank ROMOSS SENSE8+ 30000mAh = 30$
    - USB C output: 5V=3A, 9V=2A, 12V=1. 5A
    - 1 USB type C, 2 USB type A, 1 Micro USB, 1 Lightning
    - Will last at least 8 hours
- Pi4B  - USB-C / 5V at 3A
- Speaker – probably require small amount of power, if drawn from Pi, no datasheet.
- Mic - No Datasheet, not entierly sure if it will work w/o driver.
- Display – With pi, it will require 2.5 A.

# 2021-09-18 DIY power bank? <a name= "2">

The battery for 20000 mAh already costs 20$ so it seems better to just buy cheap power bank. 
Decided to build the power bank since it is rather important in our project and it would be great to have power bank with specification we want.

# 2021-10-18 First PCB assembly <a name= "3">
![Test1](FirstPCB.jpg)
![Test2](FirstPCBback.jpg)


When I was designing the new PCB, I used wrong footprints for the MOSFETS in the voltage regulator. Therefore, to test the circuit, I tried to solder the mosfets I could get which had similar parameters.
However, it seemed that the mosfets were attached incorrectly. Thus, I went ahead and test the charging part first.

# 2021-10-21 testing charger <a name= "4">

When I tested the charging circuit by inserting the battery cells and checking the voltage.

![Test3](voltagecurve.png)

I tested using a 3rd party charger and the voltage was going up similar to the curve.
When I tested with my charger, the voltage was staying the same. This meant that the circuit was not working at all.
The leds I added to the charging circuit was lighting up almost correctly which indicated that the charger was receiving correct power.

# 2021-10-25 Redesigning PCB <a name= "5">
I had quite a few problems with the first PCB, 
There were no jumpers to test currents, and too few test points to check the voltages.
I added some jumpers for the paths I thought necessary to check, and some more test points.
Also, I manually fixed the footprints of the mosfets to match the parts I ordered.
The capacitors and resistor sizes were also too small, but I just plan to reuse parts I used on the first PCB, so I just kept the footprints as is.

![Test4](newpcb.png)

# 2021-10-30 Reordering some parts. <a name= "6">
While soldering the first PCB, I lost a few parts as I ordered only one of each.
Also, I ordered wrong Micro USB-B receptacle that is not through-hole.
I reordred the LEDs for charger, some capactiors, additional USB C receptacle, and the MOSFETs for votlage regulator.
Also, I ordered speaker and display for the PI.
Reordered parts C3, d4, d3, d2, c8, q3, q4, micro usb c receptacle.

# 2021-11-02 Testing Charger <a name= "7">

Since it is impossible to test the Voltage Regulator parts without the correct MOSFETs, I decided to disconnect the Voltage regulator by desoldering a few parts that connects to the voltage regulator.
With 5V 1A wall charger and micro-USB-B cable, I tried charging a battery with starting voltage of 3.65480.
If the battery is charging correctly, the voltage should be able to reach near 4.2 or at least go up by a bit.
The LEDs lit up correctly, but the circuit does not seem to charge the battery as the voltage did not change. 
If the power bank circuit does not work at all, we will have to buy a third-party power bank that meets our need. 

# 2021-11-03 Testing Speaker and Microphone. <a name= "8">

I connected the speaker using a mono-3.5mm jack. The volume was too small.
It seems the small volume is due to lack of amplifier, and I did not consider buying an amplifier.
Also, the microphone in the ECE445 lab needs some more testing to make it work.
However, it might just be better to test some USB powered microphones since the microphone from ECE445 lab needs to be connected via 3.5mm jack.

# 2021-11-04 Audio amplifier <a name= "9">

I ordered a adafruit mono audio amplifier for our 8-ohm speaker and also a through-hole amplifier IC.
I will use the IC in protoboard to connect the speaker and make some housing if needed.
If the adafruit amplifier arrives in time, I just plan on using it for our speaker.

![Test5](amp.png)

# 2021-11-05 USB streo sound adapter? <a name= "10">

I bought a USB external stero sound adapter.
https://www.amazon.com/dp/B00IRVQ0F8?psc=1&smid=A29Y8OP2GPR7PE&ref_=chk_typ_imgToDp
This might work for our speaker and microphones but I will need to test them first.

# 2021-11-10 new PCB parts and tests <a name= "11">

The new PCB and parts arrived.

I tested the old PCB charging circuit, and it still does not seem to be charging.
I connected micro-USB-B type input at 5V 1A, and observed change of battery voltage.
At 2:18PM, Voltage was 3.65395V and at 5:56PM it was 3.65402V. Therefore, the charging circuit does not work as is.
I am planning on testing it again once I move the parts to the new PCB.

Also,
I tested voltage regulator. 2 Cells of battery having ~3.7V connected to the voltage regulator circuit, I did not solder any charging parts, and added an external ground cable between - of batteries to ground.
The voltage turns out to be almost 5V, which is desired, but the current is measured to be around 24uA. I might have made mistake in measuring the current.
I will test again for the current.

# 2021-11-11 DIY amplifier <a name= "12">

Using LM386, I tried to build a simple amplifier on a breadboard such as this.

![Test6](amp1.png)

Even though the amplifier was working, it was with many clippings and noises.

# 2021-11-12 DIY microphone <a name= "13">

I tested microphone from the lab and it does not seem to fuction correctly. 
Furthermore, I found out that the Pi does not accept microphone signal via its 3.5mm jack.
I decided to simply get a 3rd party USB powered microphone since we do not have much time to make microhpone ourselves.

# 2021-11-15 Voltage Regulator <a name= "14">

Since the voltage regulator is the most important part of the power bank, I tried to fix the voltage regulator first.
When I connected to the Pi and checked the current draw, it was only drawing 500 mA and the Pi did not turn on.

![Test11](newVR.jpg)

I tested the power using a constant current draw circuit and it seemed the maximum current draw is only .7A which is not enough to turn on the Pi.
Checking the datasheet, average max output is determined using this equation.

![Test7](eq.png)

DC = 0.16 ~ 0.26

Delta I = 1 ~ 0.7 A

Ipk = 10 A for 1.8uH inductor I have.

Then, max current draw should be around 7A.

However, Ipk is limited to 78mV/RDS(ON, N-CH).

Therefore I think the output current limit comes from wrong selection of NMOS.

![Test8](graph1.png)

NMOS RON will be high around 5V, which is what I will be running on…
The BG pin has voltage swing between 0V and Vout, thus the RON can become a problem with this mos for our design.

![Test9](graph2.png)

This is the suggested nmos, SI9804.
Also, The RDSon of PMOS is also a bit bigger at 5V.

Checking the RDSon of my parts, it was much higher than desired.
Therefore, I ordered some more mosfets with same package that has much lower RDSon.

# 2021-11-25 Voltage Regulator 2 <a name= "15">

Testing New mosfet candidates.

I replaced the N-MOS to have much lower Ron, and the PI boots up.

However, the Pi says warning saying the voltage is low.

Upon research, it is safer to have 5.1V rather than 5.0V which I set the voltage regulator to. I will try to modify the resistance for setting the voltage to change the voltage to be slightly higher.


# 2021-11-26 Voltage Regulator 3 <a name= "16">

![Test10](dia1.PNG)

In my current schematic, R1 and R2 are the same value as in the above picture but opposite. Then, Vout = 1.205*(1+316/100) = 5.0128. I was reading 5.0 V, so it makes sense. In order to increase the voltage, I need to increase the R2 or decrease R1. To decrease R1, I need about 97k. With addition of 3m3 resistor in parallel, I get about 97.06k which will give 5.13V.

With the modified circuit, I could achieve 5.098V.

Replacing PMOS to TSM260P02CX6 RFG with around 0.026 ohm ron at 4.5V.

I was keep getting the low voltage warning on Pi, so I increased voltage to 5.16. Then, I was getting no warning.


![Test12](voltagecontrol.jpg)

I connected resistor in parallel with existing smd resistor.

# 2021-11-28 Speaker Amplifier Test <a name= "17">

![Test13](testingamplifier.jpg)

Testing the amplifier circuit from Adafruit, seemed to be working correctly.
The only problem was the 5V power source it required other than the 3.5mm input signal.
Thus, I used an old USB cable and cut it to access its VDD and GND line.
With the USB power, it was working correctly. 

# 2021-11-28 Completed Speaker <a name= "18">

![Test14](speaker.jpg)

I soldered the parts of the speaker closely together. 
Also, I covered the soldered parts using heat shrink tubes to avoid possible shorts.






